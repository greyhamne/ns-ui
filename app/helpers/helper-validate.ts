//////////////////////////////////
//  Guetto helper functions
//////////////////////////////////
  
export const validateEmail = (email: string) => {
  const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return regex.exec(email) !== null 
    ? true
    : false 
}

export const validateLength = (str: string, length: number) => {
  return str.length > length 
    ? true
    : false
}

export const validateEquality = (arg1: any, arg2: any) => {
  return arg1 === arg2 
  ? true
  : false
}

export const validateEmpty = (arg1: any) => {
  return arg1.length !== 0
  ? true
  : false
}

export const validateMoney = (val) => {
  const regex  = /^\d+(?:\.\d{0,2})$/;
 
  return regex.test(parseFloat(val).toFixed(2))
    ? true
    : false
}