//////////////////////////////////
//  Gets image.ext from sting ie /path/to/image.ext
//////////////////////////////////

export const getImageName = (url: string) => {
  return url.substring(url.lastIndexOf('/')+1);
}