
interface ICreate {

   /** 
    * Collection name
    */
   collection: string,

   /**
    * Object of data
    */
   data: Object

   /**
    * If we want to use a custom ID rather
    * than firebase generating one for us, if left
    * blank 
    */
   docId?: string
}

export default ICreate;


 