/**
 * Object properties match that off NS VUE alert dialogs
 * https://nativescript-vue.org/en/docs/elements/dialogs/alert/
 */

export const MODAL = {

  GENERIC: {

    /** 
     * General error message
     */
    error: {
      title: 'Error',
      message: 'Sorry, something went wrong',
      okButtonText: 'ok gotcha'
    }
  },

  REGISTER: {

    SUCCESS: {
      title: 'Registration Successful',
      message: 'You know the drill check your emails and hit the link we sent you to begin using the app',
      okButtonText: 'ok gotcha'
    },

    FAIL: {
      title: "Registration failed",
      message: "Has the email address already been used?",
      okButtonText: "ok gotcha"
    },

    VALIDATION: {
      title: "Error",
      message: "Please fill in all fields",
      okButtonText: "ok gotcha"
    },

    PASSWORD: {
      title: "Error",
      message: "Passwords do not match",
      okButtonText: "ok gotcha"
    }

  },

  LOGIN: {
    
    SUCCESS: {
      title: "OiOi",
      message: "You have successfully logged in",
      okButtonText: "Go to home"
    },

    FAIL: {
      title: "Login Failed",
      message: 'Please try again',
      okButtonText: "Close"
    },

    VALIDATION: {
      title: "Error",
      message: "Please fill in all fields",
      okButtonText: "ok gotcha"
    },

    EMAILVERIFICATION: {
      title: "Email address",
      message: "Check your emails and verifiy you email address before proceeding",
      okButtonText: "ok gotcha"
    }

  },

  LOGOUT: {
    title: "Logout",
    message: "Are you sure",
    okButtonText: "Yes logout",
    cancelButtonText: "cancel"
  },

  PASSWORDRESET: {
    title: "Password reset",
    message: "You know how this works check your emails for details on changing your password",
    okButtonText: "ok gotcha",
  },

  PROFILE: {

    CREATED_ACCOUNT: {
      title: "Profile Complete",
      message: "Welcome to OiOi ",
      okButtonText: "Home page",
    }
    
  },

  LISTING: {

    // Using a custom overlay comp here
    // So object properties differ
    SUCCESS: {
      title: 'OiOi your listing has now been added to the Marketplace',
      buttonLabel: 'Go to home',
    },

    /** 
     * Discarding a draft confirmation
     */
    discard: {
      title: "Discard draft",
      message: "If you leave now your listing will not be saved",
      okButtonText: "Ok gotcha",
      cancelButtonText: "Wait I want to finish"
    },

    FAIL: {
      title: 'Error',
      buttonLabel: 'Failed to create listing',
    },

    EMPTY_COST: {
      title: "Error",
      message: "You must supply a cost",
      okButtonText: "OK gotcha"
    },

    RAFFLE: {
      title: "Error",
      message: "You must supply a cost",
      okButtonText: "OK gotcha"
    },

    BUY_DEFINITION: {
      message: "Simple stuff you list the item and someone buys, fee's apply",
      okButtonText: "ok gotcha"
    },

    RAFFLE_DEFINITION: {
      message: "With a raffle you determine the buy in cost as well the minimum people needed to start the raffle. Once the raffle starts it runs for 7 days then a winner is selected at random",
      okButtonText: "ok gotcha"
    },

    AUCTION_DEFINITION: {
      message: "99p starting price auction runs for 7 days highest bidder wins",
      okButtonText: "ok gotcha"
    },

    POST_DEFINITION: {
      message: "You are offering to post the item either free of charge or with a small cost to cover charges",
      okButtonText: "ok gotcha"
    },

    COLLECTION_DEFINITION: {
      message: "99p starting price auction runs for 7 days highest bidder wins",
      okButtonText: "ok gotcha"
    },

    BOTH_DEFINITION: {
      message: "99p starting price auction runs for 7 days highest bidder wins",
      okButtonText: "ok gotcha"
    },

  },

  /**  
   * Auction 
  */
 AUCTION: {

    /** 
     * User didnt bid high enough on an auction
    */
    failedBid: {
      title: "Bid Failed",
      message: "Your bid must be at least 10p more than the current winning bid",
      okButtonText: "OK gotcha"
    },

    /** 
     * successully created bid
     */
    confirmBid: {
      title: "Bid Successful",
      message: "You are now winning this item",
      okButtonText: "OK gotcha"
    },

 },

 BILLING: {
   
  /** 
   * Billing details updated
   */
   updated: {
    title: "Billing Info updated",
    message: "You can now close this window",
    okButtonText: "OK gotcha"
   },

 },

  /**
   * User has sucessfully posted a deal
   */
  DEAL: {
    success: {
      title: "Deal posted",
      message: "You can now close this window",
      okButtonText: "OK gotcha"
    },

    createAlert: {
      title: "Alert created",
      message: "You can now close this window",
      okButtonText: "OK gotcha"
    }
  }

}