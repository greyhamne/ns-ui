export const TWITCH = {

  /**
   * Base twitch url
   */
  baseUrl: 'https://api.twitch.tv',

  /**
   * Key used to authentic calls
   */
  clientKey: 'zm9y00070a3i78xtdx4hy8gon8ltz7',

  /** 
   * URL to append if you want to search twitch for games
   */
  searchGames: '/kraken/search/games?type=suggest&query=',

  /** 
   * Informartion on a particular game
   */
  getGame: '/helix/games?id=',

  /**
   * Get streams on a game
   */
  getStreams: '/kraken/streams/?game_id=',
}

