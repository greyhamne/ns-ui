/**
 * Frames based on nativescript
 * https://nativescript-vue.org/en/docs/elements/components/frame/
 */

export const FRAME = {
  MAIN: 'main'
}