import { FIREBASE } from '~/constants/constant-firebase'
import { FRAME } from '~/constants/constant-frame'
import { MODAL } from '~/constants/constant-modal'
import { TWITCH } from '~/constants/constant-twitch'
import { STRIPE } from '~/constants/constant-stripe'
import { GENERAL } from '~/constants/constant-general'
import { LISTING } from '~/constants/constant-listing'
import { ACTIVITY } from '~/constants/constant-activity'

export const CONSTANT = {
  FIREBASE,
  FRAME,
  MODAL,
  TWITCH,
  STRIPE,
  GENERAL,
  LISTING,
  ACTIVITY,
}