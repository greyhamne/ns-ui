/**
 * Activity
 */

export const ACTIVITY = {

  /**
   * Object of all possible actions
   */
  ACTIONS: {

    /**
     * General message about a listing via marketplace
     */
    message: 'MESSAGE',

    /**
     * An item is ending soon
     */
    ending: 'ENDING',

    /**
     * Entering a raffle
     */
    raffle_entry: 'RAFFLE_ENTRY',

    /**
     * A raffle has started
     */
    raffle_started: 'RAFFLE_STARTED',

    /**
     * A bid on an auction
     */
    auction_bid: 'AUCTION_BID',

    /**
     * A new deal match
     */
    deal_match: 'DEAL_MATCH',

    /**
     * User thanks
     */
    thanks: 'THANKS',
    
  },

  /**
   * General
   */
  LISTING: {

    /**
     * Message sent from the marketplace
     */
    message: {
      heading: (user) => `New message received from ${user}`,
      body: (listing) => `Regarding your listing ${listing}`
    },

    /**
     * Warning that an item ends in X hours
     */
    ending: {
      heading: (listing) => `Item ending soon: ${listing}`,
      body: (time) => `Item ends in ${time}`
    },

    /**
     * Informing user an item has sold listing/raffle/auction
     */
    sold: {
      heading: 'OiOi Listing sold',
      body: (listing, price) => `${listing} has sold for ${price}`,
    },

    /**
     * When an item fails to sell
     */
    failed: {
      heading: 'Failed to sell',
      body: (listing) => `${listing} has failed to sell`
    }
  },

  /**
   * Notifications regarding auctions
   */
  AUCTION: {

    /**
     * When a user has bid on an item
     */
    bid: {
      heading: (listing) => `New bid received: ${listing}`,
      body: (bidder, bid) => `${bidder} has bid ${bid} on your listing`,
    }
  },

  /**
   * Notifications regarding raffles
   */
  RAFFLE: {

    /**
     * A raffle has been triggered ie all criteria met
     */
    start: {
      heading: `Raffle started`,
      body: (listing) => `Raffle for ${listing} has now begun`
    },

    /**
     * User enters a raffle
     */
    entry: {
      heading: `New raffle entry`,
      body: (user, listing) => `${user} has entered the raffle for ${listing}`
    }
  },

  /**
   * Deal alert
   */
  DEALS: {

    /**
     * Found a deal that matches
     */
    match:{ 
      heading: `New deal matches`,
      body: (title, price) => `A deal for ${title} under ${price} has been added`
    },

    /**
     * User saying thanks for posting a deal
     */
    thanks: {
      heading: (author) => `Thank you ${author}`,
      body: (user, title) => `${user} says thanks for the deal you posted ${title}`
    }
    
  }
  
 }