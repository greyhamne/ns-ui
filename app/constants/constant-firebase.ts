/**
 * Firebase constants
 */

export const FIREBASE = {

  /** 
   * Firebase bucket url
   */
  BUCKET: 'gs://oioi-nativescript.appspot.com',

  /**
   * Where images are stored
   */
  UPLOADS: 'uploads',

  /** 
   * Just a string reference to help build a path ie uploads/users  
   */
  USERS: 'users',

  /** 
   * Firebase collections
   */
  COLLECTIONS: {

    /** 
     * Where listings are stored
     */
    listings: 'listings',

    /**
     * Additional user details
     */
    users: 'users',

    /**
     * Where deals are stored
     */
    deals: 'deals',

    /**
     * User who create an alert for themselves
     */
    dealAlerts: 'dealAlerts'
  },

  /**
   * Fields in a collection that are repeated a lot
   */
  FIELDS: {
    notifications: 'notifications'
  },

  FUNCTIONS: {
    STRIPE_PAYMENT: 'https://us-central1-oioi-nativescript.cloudfunctions.net/stripePayment'
  }
}

