/**
 * Fee taken for a listing ie 5%
 */
export const listingFee = 0.05;

export const LISTING = {

  /**
   * Fee deducted for use of service
   */
  fee: 0.05,

  /**
   * Image limited per listing
   */
  imageLimit: 15,

  /**
   * Defined actions for bottom nav New
   */
  type: [
    'Listing',
    'Deal',
    'Deal Alert',
  ]

  
}


//////////////////////////////////
//  Used to reduce code on listing page
//////////////////////////////////

// Listing config
export const listingConfigType = [
  {
    title: 'SINGLE',
    image: '~/assets/images/'
  },
  {
    title: 'BUNDLE',
    image: '~/assets/images/'
  }
];

// Listing config for single item condition
export const listingConfigCondition = [
  {
    title: 'LOOSE',
    image: '~/assets/images/'
  },
  {
    title: 'BOXED',
    image: '~/assets/images/'
  },
  {
    title: 'CIB',
    image: '~/assets/images/'
  }
];

// Listing config for previously sold items
export const listingConfigSold = [
  {
    title: 'LOWEST',
    image: '~/assets/images/',
    cost: '1.99'
  },
  {
    title: 'HIGHEST',
    image: '~/assets/images/',
    cost: '22.99'
  },
  {
    title: 'AVERAGE',
    image: '~/assets/images/',
    cost: '12.34'
  }
];

// Listing config for selling method
export const listingConfigMethod = [
  {
    title: 'BUY',
    image: '~/assets/images/',
  },
  {
    title: 'AUCTION',
    image: '~/assets/images/',
  },
  {
    title: 'RAFFLE',
    image: '~/assets/images/',
  }
];

// Listing config for selling method
export const listingConfigDelivery = [
  {
    title: 'POST',
    image: '~/assets/images/',
  },
  {
    title: 'COLLECTION',
    image: '~/assets/images/',
  },
  {
    title: 'BOTH',
    image: '~/assets/images/',
  }
];