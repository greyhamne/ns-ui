import firebase from 'nativescript-plugin-firebase'
import { CONSTANT } from '~/constants/constant-index'

interface ICreate {
  collection: string,
  data: any, // needs to be an object
  docId?: string
}

interface IFind {
  collection: string,
  docId: string
}

interface IUpdate {
  collection: string,
  data: any,
  docId: string,
}

interface IDelete {
  collection: string,
  docId: string
}

interface IWhere {
  collection: string
}

interface ICriteria {
  field: string,
  operator: any, // '<' | '<=' | '==' | '>=' | '>' | 'array-contains'
  value: string | number | boolean,
  limit?: string | number
}

const general = {

  /**
   * CREATE a resource
   */
  create: async(arg: ICreate, timestamp: boolean) => {

    timestamp
      ? arg.data.published = firebase.firestore.FieldValue.serverTimestamp()
      : null

    return await firebase
      .firestore
      .collection(arg.collection)
      .doc(arg.docId)
      .set(arg.data)
  },

  /** 
   * UPDATE a resource
   */
  update: async(arg: IUpdate) => {
    return await firebase
      .firestore
      .collection(arg.collection)
      .doc(arg.docId)
      .update(arg.data)
  },

  /** 
   * UPDATE a resource array
   * Set notification to true if you want a read/unread boolean
   */
  updateArr: async(arg: IUpdate, field: string) => {

    // TODO cant add firebase timestamp in array union needs a workaround
    // Timestamp
    arg.data.created = new Date().getTime()

    // If we have a call to a notifications field lets add a read status
    arg.data.collection == CONSTANT.FIREBASE.FIELDS.notifications
      ? arg.data.read = false
      : null

    return await firebase
      .firestore
      .collection(arg.collection)
      .doc(arg.docId)
      .update({
        [field]: firebase.firestore.FieldValue.arrayUnion(arg.data),
      })
      .catch((error) => {
        console.log (`general.updateArr error: ${error}`)
      })
  },

  /** 
   * Remove a resource array
   */
  removeArr: async(arg: IUpdate, field: string) => {
    return await firebase
      .firestore
      .collection(arg.collection)
      .doc(arg.docId)
      .update({
        [field]: firebase.firestore.FieldValue.arrayRemove(arg.data)
      })
      .catch((error) => {
        console.log (`general.removeArr error: ${error}`)
      })
  },

  /** 
   * GET a collection
   */
  get: async(arg: IWhere) => {

    let results = []

    await firebase
      .firestore
      .collection(arg.collection)
      .get()
      .then(querySnapshot => {

        querySnapshot.forEach(doc => {
          console.log(doc.data())
          results.push({
            id: doc.id,
            data: doc.data()
          })
        })
      })
      .catch((error) => {
        console.log (`general.removeArr error: ${error}`)
      })

      return results
  },

  /**
   * FIND a document by string provided
   */
  find: async(arg: IFind) => {
    return await firebase
      .firestore
      .collection(arg.collection)
      .doc(arg.docId)
      .get()
      .then(doc => {
        return doc.exists 
          ? doc.data()
          : console.log("Document doesnt exist in general.find()")
        
      })
      .catch((error) => {
        console.log(`general.find() error: ${error}`)
      })
  },

  /**
   * WHERE document
   */
  where: async(arg: IWhere, criteria: ICriteria) => {

    let results = []

    await firebase
      .firestore
      .collection(arg.collection)
      .where(criteria.field, criteria.operator, criteria.value)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          results.push({
            id: doc.id,
            data: doc.data()
          })
        });
      });

    return results
  },

  /**
   * DELETE a document
   */
  delete: async(arg: IDelete) => {
    return await firebase
      .firestore
      .collection(arg.collection)
      .doc(arg.docId)
      .delete()
      .then(() => {
        console.log(`document with id:${arg.docId} deleted successfully`)
      })
      .catch((error) => {
        console.log(`document with id:${arg.docId} failed to delete error: ${error} `)
      })
  },
}

export default general;

