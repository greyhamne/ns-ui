import firebase from 'nativescript-plugin-firebase'
import {EventBus} from '~/eventbus'

interface ICollection {
  collection: string,
}

interface IDoc {
  collection: string,
  docId: string,
}
 
const listener = {
  
  /** 
   * ADD a watch to a certain collection
   */
  collection: async(arg: ICollection) => {
    return firebase
      .firestore
      .collection(arg.collection)
      .onSnapshot(
        {includeMetadataChanges: true}, 
        (snapshot: any) => {
          EventBus.$emit('EventDealChange',snapshot.docSnapshots[snapshot.docSnapshots.length -1].data());
        }
      );
  },

  /** 
   * ADD a watch to a certain collection
   */
  doc: async(arg: IDoc) => {
    return firebase
      .firestore
      .collection(arg.collection)
      .doc(arg.docId)
      .onSnapshot(
        {includeMetadataChanges: true}, 
        (doc: any) => {
          EventBus.$emit('GetData',doc.data());
        }
      );
  }
}

export default listener
