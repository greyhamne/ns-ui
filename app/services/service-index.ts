/**
 * Global import of firebase/twitch services
 */

import general from '~/services/service-general'
import listener from '~/services/service-listener'
import user from '~/services/service-user'
import image from '~/services/service-image'

const service = {
  general,
  listener,
  user,
  image
}

export default service