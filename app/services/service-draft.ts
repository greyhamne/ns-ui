/**
 * TODO
 * 2. Logic for ios and android images
 * 3. How are we going to handle image deletion when a draft has been discarded
 * 
 * todo DRY
 */

import firebase from 'nativescript-plugin-firebase'
import { CONSTANT } from '~/constants/constant-index'
import { getImageName } from '~/helpers/helper-getImageName'

/**
 * Get ID of user currently logged in
 */
let currentUserId;
let userAcc
let seller
let listing

firebase.getCurrentUser()
  .then((user) => {
    // Get current users id
    currentUserId = user.uid,

    // Commonly used data
    seller = {
      uid:user.uid,
      displayName: user.displayName,
      photoURL: user.photoURL
    }

    userAcc = firebase.firestore.collection(CONSTANT.FIREBASE.COLLECTIONS.users).doc(currentUserId)

    listing = firebase.firestore.collection(CONSTANT.FIREBASE.COLLECTIONS.listings)

  })
  .catch((error) => {
    console.log(error)
  })

/**
 * Service
 */
export const serviceDraft = {
  
  /** 
   * CREATE listing draft
   */
  create: async() => {
    console.log(listing)

    return await listing
      .add({
        isDraft: true,
      })
      .then((result) => {
        return result.id
      })
  },

  /** 
   * DELETE listing
   * todo remove all pics when draft is deleted
   */
  delete: async(draftId) => {
    return await listing
      .doc(draftId)
      .delete()
      .then(() => {
        console.log('Draft deleted')
      });
  },

  /** 
   * ADD image to draft
   */
  addImage: async(image, draftid) => {

    const imagePath = image.img.src._android
    const imageNameAndExt = getImageName(imagePath)
  
    return await firebase.storage.uploadFile({
  
      remoteFullPath: `${CONSTANT.FIREBASE.UPLOADS}/${CONSTANT.FIREBASE.COLLECTIONS.listings}/${currentUserId}/${draftid}/${imageNameAndExt}`,
      localFullPath: image.img.src._android,
      
      // TODO leaving a note for myself, 
      // considering using VUEX to handle % of 
      // progress of upload to feedback to UI
  
      onProgress: function(status) {
        console.log("Uploaded fraction: " + status.fractionCompleted);
        console.log("Percentage complete: " + status.percentageCompleted);
      }
  
    }).then(
  
      function (uploadedFile) {
        console.log("File uploaded: " + JSON.stringify(uploadedFile));
        return uploadedFile;
      },
  
      function (error) {
        console.log("File upload error: " + error);
      }
  
    )
    .catch((error) => {
      console.log(`error: ${error}`)
    });
  },


  /**
   * GET an image
   */
  getImage: async(image, draftid) => {

    const imagePath = image.img.src._android
    const imageNameAndExt = getImageName(imagePath)
  
    return await firebase.storage.getDownloadUrl({
      remoteFullPath: `${CONSTANT.FIREBASE.UPLOADS}/${CONSTANT.FIREBASE.COLLECTIONS.listings}/${currentUserId}/${draftid}/${imageNameAndExt}`
    }).then(
      function (url) {
        console.log(url)
        return url
      },
      function (error) {
        console.log("Error: " + error);
      }
    )
  },

  /**
   * ADD and GET image urls
   */
  addAndGetImage: async(image, draftid) => {

    await serviceDraft
      .addImage(image, draftid)

    return serviceDraft
      .getImage(image, draftid)
      .then((result) => {
        return result
      })
  },

  /**
   * Delete single image from firestore
   * todo test this
   */
  removeImage: async(image, draftid) => {

    const imageNameAndExt = getImageName(image)
  
    return await firebase.storage.deleteFile({
      remoteFullPath: `${CONSTANT.FIREBASE.UPLOADS}/${CONSTANT.FIREBASE.COLLECTIONS.listings}/${currentUserId}/${draftid}/${imageNameAndExt}`
    }).then(
      function () {
        console.log("File deleted.");
      },
      function (error) {
        console.log("File deletion Error: " + error);
      }
    );
  
  },
  
  /** 
   * Create a listing and send to marketplace
   * TODO - need a smarter of way of not just populating empty fields
   */
  createListing: async(data, docid) => {

    console.log('createListing medthod called')
    console.log(seller)

    let listingData = {
      ...data,
      seller,

      bids: [],
      winningBid: null,
      published: firebase.firestore.FieldValue.serverTimestamp(),
    }

    return await listing
      .doc(docid)
      .update(listingData)
  },
}
