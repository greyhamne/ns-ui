import firebase from 'nativescript-plugin-firebase'
 
interface IUpload {
  image: any,
  remote: string,
  local: string
}

const image = {

  /**
   * UPLOAD image to firestore
   * NOTE: this does not return the download URL
   * will happen in seperate method 
   */
  upload: async(data: IUpload) => {
    return  await firebase.storage.uploadFile({

        /**
         * Path on firestore where image will be stored
         * NOTE: include image name and extension
         */
        remoteFullPath: data.remote,

        /**
         * Local device path
         */
        localFullPath: data.local,

        /**
         * Progress of image upload 
         */
        onProgress: (status) => {
          console.log("Uploaded fraction: " + status.fractionCompleted);
          console.log("Percentage complete: " + status.percentageCompleted);
        }
    })
    .then(
      (uploadedFile) => {
        return uploadedFile
      },

      (error) => {
        console.log(`image.upload() error: ${error}`)
      }
    )
  },

  /**
   * GET download url of an image
   */
  get: async(path: string) => {
    return await firebase
      .storage
      .getDownloadUrl({
        remoteFullPath: path
      })
      .then(
        (url) => {
          return url
        },

        (error) => {
          console.log("Error: " + error);
        }
      )
  },
  
  /**
   * DELETE an image
   */
  delete: async(path: string) => {
    return await firebase.storage.deleteFile({

      // the full path of an existing file in your Firebase storage
      remoteFullPath: path
    }).then(
        () => {
          console.log("File deleted.");
        },

        (error) => {
          console.log("File deletion Error: " + error);
        }
    );
  }
}

export default image;
