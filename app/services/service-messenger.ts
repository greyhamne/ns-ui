import firebase from 'nativescript-plugin-firebase'

let currentUserId;

firebase.getCurrentUser()
  .then((user) => {
    currentUserId = user.uid
  })
  .catch((error) => {
    console.log(error)
  })

export const serviceMessenger = {

  /** 
   * CREATE a chat between 2 users
   */
  create: async(listingid, seller) => {

    const path = `/listings/${seller.uid}/${listingid}/chats/${currentUserId}/`;
    return await  firebase.setValue(
      path,
      {

      }
    );
  },

  /** 
   * CREATE a chat between 2 users
   */
  getMessages: async(listingid, seller) => {

    const path = `/listings/${seller.uid}/${listingid}/chats/${currentUserId}/`;
    
    return await firebase.getValue(path)

  },

  /**
   * LISTEN for changes
   */
  listenForMessages: async(event, seller, id) => {
    return await firebase.addChildEventListener(event, `/listings/${seller.uid}/${id}/chats/${currentUserId}/`)
      .then((listenerWrapper) => {
          let path = listenerWrapper.path;
          let listeners = listenerWrapper.listeners; // an Array of listeners added
          // you can store the wrapper somewhere to later call 'removeEventListeners'
      });
  }

}