import firebase from 'nativescript-plugin-firebase'

export const serviceAuth = {

  /** 
   * REGISTER a user
  */
  register: async(email, password) => {
    return await firebase.createUser({
      email,
      password
    })
  },

  /** 
   * SEND verification email
  */
  verify: async() => {
    return await firebase.sendEmailVerification()  
      .then(
        function () {
          console.log("Email verification sent");
        },
        function (error) {
          console.log("Error sending email verification: " + error);
        }
      );
  },

  /** 
   * LOGIN a user
  */
  login: async(email, password) => {
    console.log('trigger login')
    return await firebase.login({
      type: firebase.LoginType.PASSWORD,
      passwordOptions: {
        email,
        password
      }
    })
  },

  /** 
   * REGISTER, LOGIN to Firebase & SEND email verification
  */
  registerAndSendVerificationEmail: async(email, password) => {
    await serviceAuth.register(email, password)
    await serviceAuth.login(email, password)
    await serviceAuth.verify()
    return serviceAuth.logout()
  },

  /**
   * RESET a users password via Firebase
   */
  resetPassword: async(email) => {
    return await firebase.sendPasswordResetEmail(email)
    .then(() => console.log("Password reset email sent"))
    .catch(error => console.log("Error sending password reset email: " + error));
  },

  /** 
   * LOGOUT a user from firebase
   */
  logout: async() => {
    return await firebase.logout()
  },
}
