import firebase from 'nativescript-plugin-firebase'
import { CONSTANT } from '~/constants/constant-index'

const user = {

  /**
   * GET user data
   */
  getUserData: async() => {
    return await firebase
      .getCurrentUser()
      .then((user) => {
        console.log(user)
        return user
      })
      .catch((error) => {
        console.log(`getUserData() error: ${error}`)
      })
  },

  /** 
   * CHECK to see if a username is available
   */
  checkUsernameAvailibility: async(username: string) => {

    return await firebase
      .firestore
      .collection(CONSTANT.FIREBASE.COLLECTIONS.users)
      .where("username", "==", username)
      .get()
      .then((querySnapshot: any) => {

        // TODO also could do with a better approach
        return querySnapshot.docSnapshots.length > 0
          ? false
          : true
        
      })
  },

  /** 
   * UPDATE users display name via Firebase
   */
  updateUserAuthData: async(data) => {
    return await firebase.updateProfile(data)
      .then(
        () => {
          console.log('Username updated')
        },

        (errorMessage) => {
          console.log(errorMessage);
        }
    );
  },

  /** 
   * GET a users metadata
   */
  userMetaData: async(uid: string) => {
    return await firebase
      .firestore
      .collection(CONSTANT.FIREBASE.COLLECTIONS.users)
      .doc(uid)
      .get()
      .then(doc => {
        return doc.exists
          ? doc.data()
          : console.log('no user meta')
    });
  },
}

export default user;


