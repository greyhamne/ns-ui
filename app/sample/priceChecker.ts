import { ObservableArray } from 'data/observable-array';
import { TokenModel } from 'nativescript-ui-autocomplete';

export const getGames = () => {
  let games = new ObservableArray();
  games.push(
    new TokenModel('Super Mario Bros', 'image'),
  );
  games.push(
    new TokenModel('Super Mario Bros 2', 'image'),
  );
  games.push(
    new TokenModel('Super Mario Bros 3', 'image'),
  );
  return games;
};