import { ObservableArray } from 'data/observable-array';
import { TokenModel } from 'nativescript-ui-autocomplete';


export const getCountries = () => {
  let countries = new ObservableArray();

  countries.push(
    new TokenModel('Mario Bros', 'https://via.placeholder.com/50'),
  );

  countries.push(
    new TokenModel('Mario Bros 2', 'https://via.placeholder.com/50'),
  );

  countries.push(
    new TokenModel('Mario Bros 3', 'https://via.placeholder.com/50'),
  );

  return countries;
};