/**
 * Main.ts
 */
import Vue from 'nativescript-vue';
import App from '~/components/App.vue';
import { EventBus } from '~/eventbus';

import firebase from 'nativescript-plugin-firebase'

import {TNSFontIcon, fonticon} from 'nativescript-fonticon';
import RadAutoCompletePlugin from 'nativescript-ui-autocomplete/vue';
import RadListView from 'nativescript-ui-listview/vue';
import Pager from 'nativescript-pager/vue';
import RadDataForm from 'nativescript-ui-dataform/vue';
import DateTimePicker from "nativescript-datetimepicker/vue";
import { LoadingIndicator } from 'nativescript-loading-indicator';

import { CONSTANT } from '~/constants/constant-index'
import service from '~/services/service-index'

/** 
 * Plugins
 */
Vue.use(RadAutoCompletePlugin);
Vue.use(RadListView);
Vue.use(Pager);
Vue.use(RadDataForm);
//@ts-ignore
Vue.use(DateTimePicker);

/** 
 * Components
 */
Vue.registerElement('Fab',() => require('nativescript-floatingactionbutton').Fab);
Vue.component('Ui-Card', () => import('~/components/ui/Ui-Card.vue'));
Vue.component('Ui-RecentSearches', () => import('~/components/ui/Ui-RecentSearches.vue'));
Vue.component('Ui-Select', () => import('~/components/ui/Ui-Select.vue'));
Vue.component('Ui-ActionBar', () => import('~/components/ui/Ui-ActionBar.vue'));
Vue.component('Ui-CameraRoll', () => import('~/components/ui/Ui-CameraRoll.vue'));
Vue.component('Ui-Modal', () => import('~/components/ui/Ui-Modal.vue'));
Vue.component('Ui-Spinner', () => import('~/components/ui/Ui-Spinner.vue'));
Vue.component('Layout-NonAuth', () => import('~/components/layout/Layout-NonAuth.vue'));
Vue.component('Ui-BottomNav', () => import('~/components/ui/Ui-BottomNav.vue'));
Vue.component('Ui-Message', () => import('~/components/ui/Ui-Message.vue'));
Vue.component('Ui-Notification', () => import('~/components/ui/Ui-Notification.vue'));
Vue.component('Ui-ListItem', () => import('~/components/ui/Ui-ListItem.vue'));
Vue.component('Ui-SearchTwitch', () => import('~/components/ui/Ui-SearchTwitch.vue'));
Vue.component('Ui-Logo', () => import('~/components/ui/Ui-Logo.vue'));
Vue.component('Ui-Deal', () => import('~/components/ui/Ui-Deal.vue'));

/**
 * Partials - Seperate tab content to reduce page bloat
 */
Vue.component('Partial-DisplayPic', () => import('~/components/partials/Partial-UpdateDisplayPic.vue'));
Vue.component('Partial-WatchedItems', () => import('~/components/partials/Partial-WatchedItems.vue'));
Vue.component('Partial-MyItems', () => import('~/components/partials/Partial-MyItems.vue'));
Vue.component('Partial-MyAlerts', () => import('~/components/partials/Partial-MyAlerts.vue'));
Vue.component('Partial-MyDeals', () => import('~/components/partials/Partial-MyDeals.vue'));
Vue.component('Partial-Listings', () => import('~/components/partials/Partial-Listings.vue'));
Vue.component('Partial-Deals', () => import('~/components/partials/Partial-Deals.vue'));

/** 
 * Make Loader global
 */
Vue.prototype.$LOADER = new LoadingIndicator();

/** 
 * Single source of truth for constants
 */
Vue.prototype.$CONSTANT = CONSTANT

/** 
 * Single source of truth for services
 */
Vue.prototype.$SERVICE = service

/** 
 * Prints Vue logs when --env.production is *NOT* set while building
 */
Vue.config.silent = (TNS_ENV === 'production');

/**
 * Custom fonts
 */
TNSFontIcon.paths = {'fa': './assets/font-awesome.css'};  
TNSFontIcon.loadCss();
Vue.filter('fonticon', fonticon);

/**
 * Init Firebase (this is not async)
 */
firebase.init({

  storageBucket: CONSTANT.FIREBASE.BUCKET,

  onAuthStateChanged: (data) => { 

    console.log(data.loggedIn ? "Logged in to firebase" : "Logged out from firebase");

    if (data.loggedIn) {
      console.log("Logged in as: " + (data.user.email ? data.user.email : "N/A"));
      Vue.prototype.$UID = data.user.uid 
      Vue.prototype.$USER = data.user
      console.log(`uid: ${data.user.uid}`)
      //console.log(data.user)

      EventBus.$emit('UserAuthenticated');
    } 
    
  },

})
.then((res) => {
    console.log("firebase.init done");
  },
  error => {
    console.log(`firebase.init error: ${error}`);
  }
);

/**
 * Init Vue instance
 */
new Vue({
  render: h => h('frame', [h(App)])
}).$start();
  